var jsonpClient = require('jsonp-client');


module.exports.getUrl = function(url, cb)
{
    jsonpClient('http://www.onsen.ag/data/api/getMovieInfo/' + url, function(err, data)
    {
        if(err)
        {
            console.log('error: ' + err);
        }
        cb(data.moviePath.pc);
    });
};
