"use strict";

var http = require('http');
var Parser = require('htmlparser2').Parser;
var fs = require('fs');

module.exports = function(cb)
{
    //set up the parser
    var items = [];
    var parser = new Parser(
    {
        onopentag: function(name, attribs)
        {
            if(attribs.id === 'movieList')
            {
                this.onopentag = function(name, attribs)
                {
                    if(name === 'li' && attribs.class && attribs.class.indexOf('clr') !== -1)
                    {
                        console.log(attribs.id);
                        items.push(attribs.id);
                    }
                };
            }
        },
        onend: function()
        {
            cb(items);
        }
    });
    
    
    http.get('http://www.onsen.ag/', function(res)
    {
        res.on('data', function(chunk)
        {
            parser.write(chunk);
        });
        res.on('end', function()
        {
            parser.end();
        });
    });
};
